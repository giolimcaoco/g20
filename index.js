console.log("JavaScript - Repetition Control Structures Activity");

let userNumber=prompt("Enter a number");
console.log("The number you provided is "+userNumber);

for (let count = userNumber; count >=0; count--){

	if (count <=50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	} 
	if (count % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}

	if (count % 5 === 0){
		console.log(count);
		continue;
	}

}



let myString="supercalifragilisticexpialidocious"
let vowel = [ 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' ];
let x = "";

console.log(myString);

	for(let i = 0; i < myString.length; i++){
		
		if (!vowel.includes(myString[i]))
		{
			x += myString[i];
		}
	}
	console.log(x);




